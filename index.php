<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="yandex-verification" content="26516d3dfc42cc17" />
    <link rel="icon" href="favicon.ico">

    <title>Tezlab | Разработка и продвижение интернет-проектов</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="fa/css/font-awesome.min.css">
    <!-- Custom styles for this template -->
    <link href="style.css" rel="stylesheet">

    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="site-wrapper">

      <div class="container">
        <header>
          <div class="row">
            <div class="col-lg-4 col-lg-offset-4 col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3">
              <div class="row">
                <div class="col-md-12">
                  <div class="logo"></div>
                  <div class="logo-text">Tezlab</div>
                </div>
              </div>           
              <div class="row">
                <div class="col-md-12 text-center">
                  <h2>eCommerce solutions</h2>
                </div>
              </div>
            </div>
          </div>
        </header>
        <section class="main">
          <div class="row">
            <div class="col-md-12">
              <h1>Разработка и продвижение<br> интернет - проектов</h1>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-4 col-lg-offset-4 contacts">
              <h4>
                <a href="tel:+78122431503"><i class="fa fa-phone"></i>+7(812) 243-15-03</a>
              </h4>
              <!--<h4>
                <a href="mailto:info@rise.uz"><i class="fa fa-envelope-o"></i> info@tezlab.ru</a>
              </h4>-->
            </div>
          </div>
        </section>
        <footer>
          <div class="row">
            <div class="col-md-4 col-md-offset-4 copy">
              © <?php echo date('Y');?> Tezlab
            </div>
          </div>
        </footer>
      </div>

    </div>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter44622565 = new Ya.Metrika({
                        id:44622565,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/44622565" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'fKYf31a6qK';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->
  </body>
</html>
